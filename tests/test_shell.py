from unittest import TestCase

from rhi.rhi import shell

class TestShell(TestCase):
    def test(self):
        self.assertTrue(shell('test 1 -eq 1'))
        self.assertFalse(shell('test 1 -ne 1'))
