import itertools
from abc import ABC, abstractclassmethod
from itertools import chain
from unittest import TestCase
from unittest.mock import MagicMock

from rhi.rhi import rhi

def case(number):
    def decorator(test_func):
        def wrapped_func(self):
            for i in range(number):
                self.up.reset_mock()
                self.down.reset_mock()
                next(self.test)
            test_func(self)
        return wrapped_func
    return decorator

class ARhiTest(ABC):
    @classmethod
    def setUpClass(cls):
        cls.fall = 0
        cls.rise = 0
        cls.cases = []

    def setUp(self):
        self.up = MagicMock()
        self.down = MagicMock()
        self.test = rhi(self.check, self.fall, self.rise, self.up, self.down)
        self._cases = itertools.chain(self.cases)

    def check(self):
        return next(self._cases)

class TestRhi(ARhiTest, TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fall = 3
        cls.rise = 1
        cls.cases = [True, False, False, False, True, True]

    @case(1)
    def test_1(self):
        self.up.assert_called_once_with()
        self.down.assert_not_called()

    @case(2)
    def test_2(self):
        self.up.assert_not_called()
        self.down.assert_not_called()

    @case(3)
    def test_3(self):
        self.up.assert_not_called()
        self.down.assert_not_called()

    @case(4)
    def test_4(self):
        self.up.assert_not_called()
        self.down.assert_called_once_with()

    @case(5)
    def test_5(self):
        self.up.assert_called_once_with()
        self.down.assert_not_called()

    @case(6)
    def test_6(self):
        self.up.assert_not_called()
        self.down.assert_not_called()
