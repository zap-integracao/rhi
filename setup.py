from setuptools.command.install import install
from setuptools import setup
import os
import os.path
import shutil
import rhi

class SystemdInstall(install):
    def run(self):
        super().run()
        cwd = os.path.dirname(os.path.realpath(__file__))
        shutil.copy(
            os.path.join(cwd, 'systemd', 'rhi.service'),
            '/lib/systemd/system/rhi.service',
        )
        if not os.path.isdir('/etc/rhi'):
            os.mkdir('/etc/rhi')
        if not os.path.isfile('/etc/rhi/rhi.conf'):
            shutil.copy(
                os.path.join(cwd, 'etc/rhi.conf'),
                '/etc/rhi/rhi.conf',
            )

setup(
    name='rhi',
    description='Route Health Injector',
    version=rhi.__version__,
    author=rhi.__author__,
    author_email=rhi.__email__,
    license=rhi.__license__,
    url='http://gitlab.zap.intra/integracao/rhi',
    packages=['rhi'],
    install_requires=['python_daemon'],
    entry_points = {
        'console_scripts': ['rhi=rhi.rhi:main'],
    },
    cmdclass={'install': SystemdInstall}
)
