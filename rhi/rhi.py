import argparse
import collections
import configparser
import functools
import logging
import shlex
import signal
import subprocess
import sys
import threading
import time
import daemon
import daemon.pidfile

from . import __version__

logger = logging.getLogger()
out = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(out)

def rhi(check, fall, rise, up, down):
    count = 0
    running = False

    while True:
        healthy = check()

        count = count + 1 if running ^ healthy else 0

        if count == (fall if running else rise):
            down() if running else up()
            count = 0
            running = not running

        yield running, healthy, count

def shell(cmd):
    return subprocess.run(
        shlex.split(cmd),
        stdout=subprocess.PIPE,
        stderr=sys.stderr,
    ).returncode == 0

class RhiThread(threading.Thread):
    @classmethod
    def from_config_section(cls, name, section):
        check = section.get('check')
        fall = section.getint('fall')
        rise = section.getint('rise')
        up = section.get('up')
        down = section.get('down')
        interval = section.getint('interval')
        assert check and fall and rise and interval, f'{name} config incomplete'
        return cls(name, check, fall, rise, up, down, interval)

    def __init__(self, name, check, fall, rise, up, down, interval):
        super().__init__(target=self._target)
        self._exit = threading.Event()
        self._rhi = rhi(
            functools.partial(shell, check),
            fall,
            rise,
            functools.partial(self._shell, up, 'UP'),
            functools.partial(self._shell, down, 'DOWN'),
        )
        self._interval = interval
        self._name = name

    def _shell(self, cmd, state):
        if cmd:
            logger.info(f'{self._name} {state}')
            return shell(cmd)

    def _target(self):
        while not self._exit.is_set():
            logger.debug(f'{self._name} {next(self._rhi)}')
            time.sleep(self._interval)

    def start(self):
        logger.info(f'{self._name} STARTED')
        super().start()

    def stop(self):
        self._exit.set()
        self.join()
        logger.info(f'{self._name} STOPPED')

class RhiDaemon(daemon.DaemonContext):
    def __init__(self, config_file, pid_file):
        super().__init__(
            pidfile=daemon.pidfile.TimeoutPIDLockFile(pid_file, timeout=1),
            signal_map = {
                signal.SIGTERM: self.stop,
                signal.SIGHUP: self.reload,
            },
            stdout=sys.stdout,
            stderr=sys.stderr,
        )
        self.config_file = config_file
        self.thread_map = self.load_config_file()

    def load_config_file(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        return {
            section: RhiThread.from_config_section(section, config[section])
            for section in config if section != 'DEFAULT'
        }

    def run(self):
        [t.start() for t in self.thread_map.values()]
        alive = True
        while alive:
            time.sleep(1)
            alive = any([t.is_alive() for t in self.thread_map.values()])

    def stop(self, signal_number, stack_frame):
        [t.stop() for t in self.thread_map.values()]

    def reload(self, signal_number, stack_frame):
        thread_map = self.load_config_file()

        for route in set(thread_map.keys()) & set(self.thread_map.keys()):
            running = self.thread_map[route]
            updated = thread_map[route]
            running.stop()
            updated.start()

        for route in set(thread_map.keys()) - set(self.thread_map.keys()):
            thread_map[route].start()

        for route in set(self.thread_map.keys()) - set(thread_map.keys()):
            self.thread_map[route].stop()

        self.thread_map = thread_map

def main():
    parser = argparse.ArgumentParser(
        prog='rhi',
        description='Route Health Injector',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '-d',
        '--daemon',
        dest='daemon',
        action='store_true',
        help='Run as daemon, otherwise load config and exit',
    )
    parser.add_argument(
        '-c',
        '--config',
        dest='config_file',
        default='/etc/rhi/rhi.conf',
        help='Configuration file',
    )
    parser.add_argument(
        '-p',
        '--pid',
        dest='pid_file',
        default='/var/run/rhi.pid',
        help='PID file',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        dest='verbose',
        action='count',
        default=None,
    )

    try:
        args = parser.parse_args()

        logger.setLevel(logging.DEBUG if args.verbose else logging.INFO)
        logger.info(f'Route Health Injector {__version__}')

        rhi_daemon = RhiDaemon(args.config_file, args.pid_file)

        if not args.daemon:
            logger.info('Config OK')
            sys.exit(0)
    except Exception as err:
        logger.error('Config ERROR')
        logger.exception(err)
        sys.exit(1)

    try:
        with rhi_daemon as d:
            d.run()
    except Exception as err:
        logger.exception(err)
        sys.exit(1)
    else:
        sys.exit(0)

if __name__ == '__main__':
    main()
