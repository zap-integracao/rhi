__author__ = "Pedro Rodrigues"
__copyright__ = "Copyright 2021, Pedro Rodrigues"
__credits__ = ["Pedro Rodrigues"]
__license__ = "GPLv2"
__version__ = "1.0"
__maintainer__ = "Pedro Rodrigues"
__email__ = "pedro.rodrigues@zap.co.ao"
__status__ = "Production"
