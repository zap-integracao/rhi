# rhi

## Installation

```
sudo pip3 install https://gitlab.zap.intra/integracao/rhi.git --install-option systemd
sudo systemctl enable rhi
sudo systemctl start rhi
```

Edit the configuration file at `/etc/rhi/rhi.conf` and restart the rhi service.
