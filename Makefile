.phony: help

# directory to store virtual environment
VENV_NAME=venv

# python runtime version
PYTHON_VER=3.8

# python executable
PYTHON?=${VENV_NAME}/bin/python${PYTHON_VER}

help:				## Shows this message.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

venv:				## Recreate the virtual environment.
venv: ${VENV_NAME}/bin/activate
${VENV_NAME}/bin/activate:
	if [ ! -d ${VENV_NAME} ]; then \
		virtualenv -p python${PYTHON_VER} ${VENV_NAME} && \
		if [ -f requirements.txt ]; then \
			${PYTHON} -m pip install -U pip -r requirements.txt; \
		fi; \
		touch $@; \
	fi;

test:				## Run the test suite.
test: venv
	${PYTHON} -m unittest discover tests/
